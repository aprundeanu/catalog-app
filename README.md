# Catalog-app
A web application meant to act as a university catalog for professors and students to manage their grades.

The authorization infrastructure has not been finalized. Plans are to finish it with service methods annotations (pre/post authorize)
amd with principal(logged user) access manipulation through CatalogAuthentication.

One admin account is created at the start of the application with credentials: \
username: admin  
password: admin