package com.catalog.app

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class CatalogApp

  object CatalogApp extends App {
    SpringApplication.run(classOf[CatalogApp])
  }

