package com.catalog.app.config

import com.catalog.app.entities.User
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

import java.util

class CatalogUserDetails(user: User) extends UserDetails{

  override def getAuthorities: util.Collection[_ <: GrantedAuthority] = {
    val authorities: util.List[SimpleGrantedAuthority] = new util.ArrayList[SimpleGrantedAuthority]()
    authorities.add(new SimpleGrantedAuthority(user.getUserRole.getAuthority))
    authorities
  }

  override def getPassword: String = user.getPassword

  override def getUsername: String = user.getUsername

  override def isAccountNonExpired: Boolean = true

  override def isAccountNonLocked: Boolean = true

  override def isCredentialsNonExpired: Boolean = true

  override def isEnabled: Boolean = user.isEnabled
}
