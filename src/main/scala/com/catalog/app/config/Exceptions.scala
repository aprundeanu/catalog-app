package com.catalog.app.config

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.{ResponseStatus}

@ResponseStatus(value = HttpStatus.NOT_FOUND)
case class NotFoundException(err: String) extends Exception(err){
}

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
case class BadPropertyException(err: String) extends Exception(err){
}

@ResponseStatus(value = HttpStatus.CONFLICT)
case class AlreadyExistsException(err: String) extends Exception(err) {
}

@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
case class UnauthorizedException(err: String) extends Exception(err)