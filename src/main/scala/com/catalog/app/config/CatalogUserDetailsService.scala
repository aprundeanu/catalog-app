package com.catalog.app.config

import com.catalog.app.entities.User
import com.catalog.app.repositories.UserRepository
import org.springframework.security.core.userdetails.{UserDetails, UserDetailsService}

import scala.jdk.OptionConverters.RichOptional

class CatalogUserDetailsService(userRepository: UserRepository) extends UserDetailsService {

  override def loadUserByUsername(s: String): UserDetails = {
    userRepository.findByUsername(s).toScala match {
      case None => throw NotFoundException("This user cannot be found in the database.")
      case Some(user) => new CatalogUserDetails(user)
    }
  }
}
