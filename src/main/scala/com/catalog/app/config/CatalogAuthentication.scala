package com.catalog.app.config
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component

@Component
class CatalogAuthentication extends AuthenticationFacade {

  override def getAuthentication: Authentication = {
    SecurityContextHolder.getContext.getAuthentication
  }
}
