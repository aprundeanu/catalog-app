package com.catalog.app.config

import org.springframework.security.core.Authentication

trait AuthenticationFacade {

  def getAuthentication: Authentication

}
