package com.catalog.app.config

import com.catalog.app.repositories.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.{Bean, Configuration}
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.{HttpSecurity, WebSecurity}
import org.springframework.security.config.annotation.web.configuration.{EnableWebSecurity, WebSecurityConfigurerAdapter}
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.context.annotation.Bean
import org.springframework.http.HttpMethod
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
class WebSecurityConfig(@Autowired val userRepository: UserRepository) extends WebSecurityConfigurerAdapter {


  @throws[Exception]
  override def configure(web: WebSecurity): Unit = {
    web.ignoring.antMatchers(HttpMethod.OPTIONS).antMatchers("/console/**", "/h2-console/**") // Disable the token authentication filter for the h2 db gui management console
      .antMatchers("/v2/api-docs", "/configuration/**", "/swagger*/**", "/webjars/**") // Swagger GUI endpoint documentation
      .antMatchers("/console", "/h2-console", "/h2-console/**", "/console/**", "/console/", "/swagger-ui.html", "/**/*.css",
        "/**/*.js", "/**/*.png", "/configuration/**", "/swagger-resources", "/v2/**")
  }

  override def configure(http: HttpSecurity) = {
    http.authorizeRequests.antMatchers("/console", "/h2-console", "/h2-console/**", "/console/**", "/console/", "/swagger-ui.html", "/**/*.css", "/**/*.js", "/**/*.png", "/configuration/**", "/swagger-resources", "/v2/**").permitAll
    http.authorizeRequests.anyRequest.authenticated
    http.formLogin().permitAll()
    http.logout().permitAll()
    http.csrf.disable
//    http.headers.frameOptions.disable
    http.httpBasic.disable()
  }

  @Bean def bCryptPasswordEncoder = new BCryptPasswordEncoder

  @Bean override def userDetailsService: UserDetailsService = new CatalogUserDetailsService(userRepository)
}