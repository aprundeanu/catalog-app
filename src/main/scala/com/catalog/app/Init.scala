package com.catalog.app

import com.catalog.app.entities.{User, UserRole}
import com.catalog.app.repositories.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.event.{ContextRefreshedEvent, EventListener}
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Component

@Component
class Init(@Autowired userRepository: UserRepository, @Autowired passwordEncoder: PasswordEncoder) {

  @EventListener
  def onApplicationEvent(event: ContextRefreshedEvent): Unit = {
    val user = new User()
    user.setId(1)
    user.setUsername("admin")
    user.setPassword(passwordEncoder.encode("admin"))
    user.setEnabled(true)
    user.setUserRole(UserRole.ADMIN)
    userRepository.save(user)
  }

}
