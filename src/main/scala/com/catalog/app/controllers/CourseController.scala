package com.catalog.app.controllers

import com.catalog.app.entities.{Catalog, Course}
import com.catalog.app.services.CourseService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.{HttpStatus, ResponseEntity}
import org.springframework.web.bind.annotation.{DeleteMapping, GetMapping, PathVariable, PostMapping, RequestBody, RequestMapping, RestController}

import java.util
import java.lang.Iterable

@RestController
@RequestMapping(path = Array("/courses"))
class CourseController(@Autowired courseService: CourseService) extends BasicController[Course](courseService){

  /**
   * Add a new course to the database.
   *
   * @param newCourse details about the new course
   * @return the newly added course
   */
  @PostMapping
  def createCourse(@RequestBody newCourse: Course): ResponseEntity[Course] = {
    courseService.createCourse(newCourse) match {
      case Left(e) => throw e
      case Right(course) => new ResponseEntity[Course](course, HttpStatus.CREATED)
    }
  }

  /**
   * Remove a course from the database.
   * @param courseId the id of the course to be removed.
   * @return a success message
   */
  @DeleteMapping(path = Array("/{courseId}"))
  def removeCourse(@PathVariable courseId: Integer): ResponseEntity[String] = {
    courseService.removeCourse(courseId) match {
      case Left(e) => throw e
      case Right(msg) => new ResponseEntity[String](msg, HttpStatus.OK)
    }
  }

  /**
   * Return all the grades for all the students that are enrolled in a specific course.
   * @param courseId the id of the course to look for students and grades.
   * @return a list of all those student grades.
   */
  @GetMapping(path = Array("/{courseId}/grades"))
  def getCourseGrades(@PathVariable courseId: Integer): ResponseEntity[Iterable[Catalog]] = {
    courseService.getCourseGrades(courseId) match {
      case Left(e) => throw e
      case Right(result) => new ResponseEntity[Iterable[Catalog]](result, HttpStatus.OK)
    }
  }

}
