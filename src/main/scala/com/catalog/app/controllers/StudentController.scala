package com.catalog.app.controllers

import com.catalog.app.entities.{Catalog, Student, UserRole}
import com.catalog.app.services.StudentService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.{HttpStatus, ResponseEntity}
import org.springframework.web.bind.annotation.{GetMapping, PathVariable, PutMapping, RequestBody, RequestMapping, RestController}

import java.lang.Iterable

@RestController
@RequestMapping(path = Array("/students"))
class StudentController(@Autowired val studentService: StudentService)
  extends EnrollmentController[Student](userRole = UserRole.STUDENT, studentService){

  /**
   * Get all the grades for a specific student.
   * @param studentId the id of the student to retrieve the grades for.
   * @return the grades of this student
   */
  @GetMapping(path = Array("/{studentId}/grades"))
  def getStudentGrades(@PathVariable studentId: Integer): ResponseEntity[Iterable[Catalog]] = {
    studentService.getStudentGrades(studentId) match {
      case Left(err) => throw err
      case Right(v) => new ResponseEntity[Iterable[Catalog]](v, HttpStatus.OK)
    }
  }

  /**
   * Get the grade that a student has for a specific course.
   * @param studentId the id of the student
   * @param courseId the id of the course
   * @return the grade achieved for this course
   */
  @GetMapping(path = Array("/{studentId}/grades/{courseId}"))
  def getStudentGradeForCourse(@PathVariable studentId: Integer, @PathVariable courseId: Integer): ResponseEntity[Catalog] = {
    studentService.getStudentGradeForCourse(studentId, courseId) match {
      case Left(err) => throw err
      case Right(value) => new ResponseEntity[Catalog](value, HttpStatus.OK)
    }
  }

  /**
   * Update the grade a student has for a specific course.
   * @param studentId the id of the student
   * @param courseId the id of the course
   * @param grade the new grade to be updated
   * @return the updated grade
   */
  @PutMapping(path = Array("/{studentId}/grades/{courseId}"))
  def updateGrade(@PathVariable studentId: Integer,
                  @PathVariable courseId: Integer,
                  @RequestBody grade: Double): ResponseEntity[Catalog] = {
    studentService.updateGrade(studentId, courseId, grade) match {
      case Left(e) => throw e
      case Right(catalog) => new ResponseEntity[Catalog](catalog, HttpStatus.OK)
    }
  }

}
