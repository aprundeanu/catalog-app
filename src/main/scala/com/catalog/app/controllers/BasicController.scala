package com.catalog.app.controllers

import com.catalog.app.entities.User
import com.catalog.app.services.BasicService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.{HttpStatus, ResponseEntity}
import org.springframework.web.bind.annotation.{GetMapping, PathVariable}

import java.lang.Iterable

abstract class BasicController[T](@Autowired basicService: BasicService[T]) {

  /**
   * Get one element from the db.
   * @return this specific element.
   */
  @GetMapping
  def getAll: ResponseEntity[Iterable[T]] = {
    new ResponseEntity(basicService.getAll, HttpStatus.OK)
  }


  @GetMapping(path = Array("/{id}"))
  def getOne(@PathVariable id: Integer): ResponseEntity[T] = {
    basicService.getOne(id) match {
      case Left(err) => throw err
      case Right(elem) => new ResponseEntity(elem, HttpStatus.OK)
    }  }

}
