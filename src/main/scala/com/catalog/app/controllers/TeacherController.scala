package com.catalog.app.controllers

import com.catalog.app.entities.{Teacher, UserRole}
import com.catalog.app.services.TeacherService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.{RequestMapping, RestController}

@RestController
@RequestMapping(path = Array("/teachers"))
class TeacherController(@Autowired teacherService: TeacherService)
  extends EnrollmentController[Teacher](userRole = UserRole.TEACHER, teacherService){

}
