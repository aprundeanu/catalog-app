package com.catalog.app.controllers

import com.catalog.app.entities.{FacultyMember, UserRole}
import com.catalog.app.services.EnrollmentService
import org.springframework.http.{HttpStatus, ResponseEntity}
import org.springframework.web.bind.annotation.{PathVariable, PostMapping, PutMapping, RequestBody, RestController}

/**
 * Class for handling account creation and course registration for different faculty members(students/teachers)
 * @param userRole the role of the faculty memeber represented with this class
 * @param enrollmentService the service handling the business logic
 * @tparam T type of the class handled by this template controller.
 */
abstract class EnrollmentController[T <: FacultyMember](userRole: UserRole, enrollmentService: EnrollmentService[T])
  extends BasicController[T](enrollmentService){

  /**
   * Create a new account with a specific userRole.
   * @param newAccount details about the new account to be created
   * @return the newly created account.
   */
  @PostMapping
  def createAccount(@RequestBody newAccount: T): ResponseEntity[T] = {
    enrollmentService.createAccount(newAccount, userRole) match {
      case Right(value) => new ResponseEntity[T](value, HttpStatus.CREATED)
      case Left(exception) => throw exception
    }
  }

  /**
   * Enroll a faculty member (student/teacher) in a course.
   * @param memberId the id of the faculty member
   * @param courseId the id of the course to enroll in
   * @return the faculty member with the updated enrolled course
   */
  @PutMapping(path = Array("/{memberId}/enroll/{courseId}"))
  def enrollInCourse(@PathVariable memberId: Integer, @PathVariable courseId: Integer): ResponseEntity[T] = {
    enrollmentService.enrollInCourse(memberId, courseId) match {
      case Left(exception) => throw exception
      case Right(element) => new ResponseEntity[T](element, HttpStatus.OK)
    }
  }

  /**
   * Unenroll a faculty member (student/teacher) from a course.
   * @param memberId the id of the faculty member
   * @param courseId the id of the course
   * @return the updated faculty member with the course removed from the list of its enrolled courses.
   */
  @PutMapping(path = Array("/{memberId}/unenroll/{courseId}"))
  def unenrollCourse(@PathVariable memberId: Integer, @PathVariable courseId: Integer): ResponseEntity[T] = {
    enrollmentService.unenrollFromCourse(memberId, courseId) match {
      case Left(exception) => throw exception
      case Right(value) => new ResponseEntity[T](value, HttpStatus.OK)
    }
  }



}
