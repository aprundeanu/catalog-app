package com.catalog.app.controllers

import com.catalog.app.config.NotFoundException
import com.catalog.app.entities.{Student, Teacher, User}
import com.catalog.app.services.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.{HttpStatus, ResponseEntity}
import org.springframework.web.bind.annotation.{DeleteMapping, GetMapping, PathVariable, PutMapping, RequestBody, RequestMapping, RestController}

import java.lang.Iterable
import java.lang.String
import java.util.Optional
import scala.None

/**
 * Endpoint segment for managing administrative details about user accounts.
 * @param userService the service handling the user related actions.
 */
@RestController
@RequestMapping(path = Array("/users"))
class UserController(@Autowired val userService: UserService) extends BasicController[User](userService){

  /**
   * Edit the administrative information about an user.
   * @param id the id of the user to edit for.
   * @param user new updated information.
   * @return the updated user.
   */
  @PutMapping(path = Array("/{id}"))
  def editUser(@PathVariable id: Integer, @RequestBody user: User): ResponseEntity[User] = {
    userService.editUser(id, user) match {
      case Left(err) => throw err
      case Right(user) => new ResponseEntity[User](user, HttpStatus.OK)
    }
  }

  /**
   * Activate the account of an user.
   * @param id the id of the user to activate the account for.
   * @return the updated user details.
   */
  @PutMapping(path = Array("/{id}/activate"))
  def activateAccount(@PathVariable id: Integer): ResponseEntity[User] = {
    userService.changeActiveStatus(id, active = true) match {
      case Left(err) => throw err
      case Right(user) => new ResponseEntity[User](user, HttpStatus.OK)
    }
  }

  /**
   * Deactivate the account of an user.
   * @param id the id of the user to deactivate the account for.
   * @return the updated user details.
   */
  @PutMapping(path = Array("/{id}/deactivate"))
  def deactivateAccount(@PathVariable id: Integer): ResponseEntity[User] = {
    userService.changeActiveStatus(id, active = false) match {
      case Left(err) => throw err
      case Right(user) => new ResponseEntity[User](user, HttpStatus.OK)
    }
  }

  /**
   * Delete the account of a specific user.
   * @param id the id of the user to have the account deleted.
   * @return status message regarding this procedure.
   */
  @DeleteMapping(path = Array("/{id}"))
  def deleteAccount(@PathVariable id: Integer): ResponseEntity[String] = {
    userService.deleteAccount(id) match {
      case Left(err) => throw err
      case Right(value) => new ResponseEntity[String]("Deleted", HttpStatus.OK)
    }
  }

}
