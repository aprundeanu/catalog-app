package com.catalog.app.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "catalog")
public class Catalog implements Serializable {

    @EmbeddedId
    @JsonIgnore
    private CatalogKey catalogKey;

    @ManyToOne(optional = false)
    @MapsId("studentId")
    @JoinColumn(name = "student_id")
    private Student student;

    @ManyToOne(optional = false)
    @MapsId("courseId")
    @JoinColumn(name = "course_id")
    private Course course;

    @Column
    private double grade;
    @Column
    private boolean passed;

    public Catalog() {
    }

    public Catalog(Student student, Course course) {
        this.catalogKey = new CatalogKey(student.getId(), course.getId());
        this.student = student;
        this.student.setEnrolledCourses(null);
        this.course = course;
        this.course.setEnrolledStudents(null);
    }

    public CatalogKey getCatalogKey() {
        return catalogKey;
    }

    public void setCatalogKey(CatalogKey catalogKey) {
        this.catalogKey = catalogKey;
    }

    public Student getStudent() {
        student.setEnrolledCourses(null);
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Course getCourse() {
        course.setEnrolledStudents(null);
        course.setResponsibleTeachers(null);
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public double getGrade() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade = grade;
        this.passed = grade >= 7.0;
    }

    public boolean isPassed() {
        return passed;
    }

    public void setPassed(boolean passed) {
        this.passed = passed;
    }
}
