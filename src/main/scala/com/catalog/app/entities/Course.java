package com.catalog.app.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "course")
public class Course implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    @NotBlank
    private String name;

    @Column
    private int edition = 1;

    @ManyToMany(mappedBy = "courses")
    @JsonIgnore
    private List<Student> students = new ArrayList<>();

    @Transient
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<Student> enrolledStudents = new ArrayList<>();

    @ManyToMany(mappedBy = "courses")
    @JsonIgnore
    private List<Teacher> teachers = new ArrayList<>();

    @Transient
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<Teacher> responsibleTeachers = new ArrayList<>();

    public Course() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEdition() {
        return edition;
    }

    public void setEdition(int edition) {
        this.edition = edition;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> enrolledStudents) {
        this.students = enrolledStudents;
    }

    public List<Teacher> getTeachers() {
        return teachers;
    }

    public void setTeachers(List<Teacher> responsibleTeachers) {
        this.teachers = responsibleTeachers;
    }

    /**
     * Load the projection field 'enrolledStudents' with
     * the corresponding desired fields from the original entity Student.
     * @return a list of projected students
     */
    public List<Student> getEnrolledStudents() {
        if(enrolledStudents == null) return null;
        enrolledStudents = List.copyOf(students);
        for(Student s: enrolledStudents) {
            s.setEnrolledCourses(null);
        }
        return enrolledStudents;
    }

    public void setEnrolledStudents(List<Student> enrolledStudents) {
        this.enrolledStudents = enrolledStudents;
    }

    /**
     * Load the projection field 'responsibleTeachers' with
     * the corresponding desired fields from the original entity Teacher.
     * @return a list of projected teachers
     */
    public List<Teacher> getResponsibleTeachers() {
        if(responsibleTeachers == null) return null;
        responsibleTeachers = List.copyOf(teachers);
        for(Teacher t: responsibleTeachers) {
            t.setAssignedCourses(null);
        }
        return responsibleTeachers;
    }

    public void setResponsibleTeachers(List<Teacher> responsibleTeachers) {
        this.responsibleTeachers = responsibleTeachers;
    }
}
