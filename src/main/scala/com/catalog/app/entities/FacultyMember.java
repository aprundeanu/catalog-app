package com.catalog.app.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Class representing a member of the faculty,
 * with an account and a relationship to the faculty's courses (enroll/unenroll)
 */
@MappedSuperclass
public abstract class FacultyMember implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    protected int id;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @MapsId
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    protected User user;

    @Transient
    protected String username;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getUsername() {
        if(this.user == null) return this.username;
        return this.user.getUsername();
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public abstract List<Course> getCourses();

    public abstract void setCourses(List<Course> enrolledCourses);

    public boolean alreadyEnrolled(int courseId) {
        return getCourses().stream().anyMatch(course -> course.getId() == courseId);
    }

    public abstract void enroll(Course newCourse);

    public abstract void unenroll(Course oldCourse);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FacultyMember that = (FacultyMember) o;
        return id == that.id && user.equals(that.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user);
    }
}
