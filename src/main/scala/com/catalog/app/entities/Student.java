package com.catalog.app.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "student")
public class Student extends FacultyMember {

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "enrolled_courses",
            joinColumns = @JoinColumn(name = "student_id"),
            inverseJoinColumns = @JoinColumn(name = "course_id"))
    @JsonIgnore
    private List<Course> courses = new ArrayList<>();

    @Transient
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private List<Course> enrolledCourses = new ArrayList<>();

    public Student() {
    }

    /**
     * Load the projection field 'enrolledCourses' with
     * the corresponding desired fields from the original entity Course.
     * @return a list of projected courses
     */
    public List<Course> getEnrolledCourses() {
        if(enrolledCourses == null) return null;
        enrolledCourses = List.copyOf(courses);
        for(Course c: enrolledCourses) {
         c.setEnrolledStudents(null);
        }
        return enrolledCourses;
    }

    public void setEnrolledCourses(List<Course> enrolledCourses) {
        this.enrolledCourses = enrolledCourses;
    }

    @Override
    public List<Course> getCourses() {
        return courses;
    }

    @Override
    public void setCourses(List<Course> enrolledCourses) {
        this.courses = enrolledCourses;
    }

    @Override
    public void enroll(Course newCourse) {
        this.getCourses().add(newCourse);
        newCourse.getStudents().add(this);
    }

    @Override
    public void unenroll(Course oldCourse) {
        this.getCourses().remove(oldCourse);
        oldCourse.getStudents().remove(this);
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", user=" + user +
                ", username='" + username + '\'' +
                ", enrolledCourses=" + courses +
                '}';
    }
}
