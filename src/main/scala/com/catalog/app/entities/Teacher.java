package com.catalog.app.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "teacher")
public class Teacher extends FacultyMember {

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "assigned_courses",
            joinColumns = @JoinColumn(name = "teacher_id"),
            inverseJoinColumns = @JoinColumn(name = "course_id"))
    @JsonIgnore
    private List<Course> courses = new ArrayList<>();

    @Transient
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private List<Course> assignedCourses = new ArrayList<>();

    public Teacher() {
    }

    /**
     * Load the projection field 'assignedCourses' with
     * the corresponding desired fields from the original entity Course.
     * @return a list of projected courses
     */
    public List<Course> getAssignedCourses() {
        if(assignedCourses == null) return null;
        assignedCourses = List.copyOf(courses);
        for(Course c: assignedCourses) {
            c.setResponsibleTeachers(null);
        }
        return assignedCourses;
    }

    public void setAssignedCourses(List<Course> assignedCourses) {
        this.assignedCourses = assignedCourses;
    }

    @Override
    public List<Course> getCourses() {
        return courses;
    }

    @Override
    public void setCourses(List<Course> enrolledCourses) {
        this.courses = enrolledCourses;
    }

    @Override
    public void enroll(Course newCourse) {
        this.getCourses().add(newCourse);
        newCourse.getTeachers().add(this);
    }

    @Override
    public void unenroll(Course oldCourse) {
        this.getCourses().remove(oldCourse);
        oldCourse.getTeachers().remove(this);
    }


}
