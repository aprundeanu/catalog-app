package com.catalog.app.entities;

import java.io.Serializable;

public enum UserRole implements Serializable {

    ADMIN,
    TEACHER,
    STUDENT;

    UserRole() {
    }

    public String getAuthority() {
        return "ROLE_" + this.name();
    }

}
