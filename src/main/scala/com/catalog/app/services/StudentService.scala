package com.catalog.app.services

import com.catalog.app.config.{BadPropertyException, NotFoundException}
import com.catalog.app.entities.{Catalog, Student}
import com.catalog.app.repositories.{StudentRepository, UserRepository}
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

import java.lang.Iterable
import scala.jdk.OptionConverters.RichOptional

@Service
class StudentService(@Autowired private val studentRepository: StudentRepository,
                     @Autowired private val courseService: CourseService,
                     @Autowired private val userRepository: UserRepository)
  extends EnrollmentService[Student](studentRepository, courseService, userRepository, elem = "student"){

  /**
   * Update the grade that a student has received for a specific course.
   * @param studentId the id of the student
   * @param courseId the id of the course
   * @param grade the grade in number format
   * @return either with the grade as a class or
   *         NotFoundException if the student/course are not in db
   *         BadPropertyException if the student is not enrolled in this course
   */
  def updateGrade(studentId: Integer, courseId: Integer, grade: Double): Either[Exception, Catalog] = {
    studentRepository.findById(studentId).toScala match {
      case None => Left(NotFoundException(elemNotFoundErrMsg))
      case Some(student) => courseService.getOne(courseId) match {
        case Left(e) => Left(e)
        case Right(course) =>
          if(!student.getCourses.contains(course)) return Left(BadPropertyException("Student is not enrolled in this course."))
          Right(courseService.updateGrade(student, course, grade))
      }
    }
  }

  /**
   * Get the grades obtained by a student.
   * @param studentId the id of the student
   * @return Either with the list of grades for this student or
   *         NotFoundException in case the student is not in db
   */
  def getStudentGrades(studentId: Integer): Either[Exception, Iterable[Catalog]] = {
    if (studentRepository.existsById(studentId)) return Left(NotFoundException(elemNotFoundErrMsg))
    Right(courseService.getStudentGrades(studentId))
  }

  /**
   * Return the student's grade for a specific course
   * @param studentId the id of the student
   * @param courseId the id of the course
   * @return the grade as a class
   */
  def getStudentGradeForCourse(studentId: Integer, courseId: Integer): Either[Exception, Catalog] = {
    if (studentRepository.existsById(studentId)) return Left(NotFoundException(elemNotFoundErrMsg))
    courseService.getStudentCourseGrade(studentId, courseId)
  }
}
