package com.catalog.app.services

import com.catalog.app.config.{AuthenticationFacade, BadPropertyException, NotFoundException}
import com.catalog.app.entities.{Catalog, Course, Student}
import com.catalog.app.repositories.{CatalogRepository, CourseRepository}
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Service

import java.lang.Iterable
import scala.jdk.CollectionConverters.CollectionHasAsScala

/**
 * Service for handling operations regarding courses and grades.
 * @param courseRepository repository managing courses.
 * @param catalogRepository repository managing grades.
 */
@Service
class CourseService(@Autowired courseRepository: CourseRepository,
                    @Autowired catalogRepository: CatalogRepository)
  extends BasicService[Course](courseRepository, elem = "course") {

  @Autowired val authentication: AuthenticationFacade = null


  /**
   * Query all the elements of type T inside the db.
   *
   * @return all the elements of type T inside the db.
   */
  override def getAll: Iterable[Course] = {
    println(authentication.getAuthentication.getName)
    super.getAll
  }

  /**
   * Create a new course inside the db.
   *
   * @param newCourse details about the new course.
   * @return the newly added course
   */
  @PreAuthorize("hasRole('ADMIN')")
  def createCourse(newCourse: Course): Either[Exception, Course] = {
    if (newCourse.getName.isBlank) Left(BadPropertyException("Course name cannot be empty!"))
    Right(courseRepository.save(newCourse))
  }

  /**
   * Remove a course from the database.
   * @param oldCourseId the id of the course to be removed
   * @return either success message or NotFoundException if it is not found
   */
  @PreAuthorize("hasRole('ADMIN')")
  def removeCourse(oldCourseId: Integer): Either[Exception, String] = {
    if(!courseRepository.existsById(oldCourseId)) Left(NotFoundException(elemNotFoundErrMsg))
    courseRepository.deleteById(oldCourseId)
    Right("Deleted")
  }

  /**
   * Retrieve all the grades that all the enrolled students have for this course
   * @param courseId the id of the course
   * @return a list of grades
   */
  def getCourseGrades(courseId: Integer): Either[Exception, Iterable[Catalog]] = {
    if(!courseRepository.existsById(courseId)) Left(NotFoundException(elemNotFoundErrMsg))
    Right(catalogRepository.findCatalogsByCourseId(courseId))
  }

  /**
   * Get the grades of a specific student.
   * @param studentId the id of the student to get the grades for.
   * @return a list of student grades
   */
  @PreAuthorize("hasRole('STUDENT') || hasRole('TEACHER')")
  def getStudentGrades(studentId: Integer): Iterable[Catalog] = {
    catalogRepository.findCatalogsByStudentId(studentId)
  }

  /**
   * Return the grade that a student has achieved for a specific course.
   * @param studentId the id of the student
   * @param courseId the id of the course
   * @return either the grade or NotFoundException in case the course does not exist inside the db.
   */
  def getStudentCourseGrade(studentId: Integer, courseId: Integer): Either[Exception, Catalog] = {
    if(!courseRepository.existsById(courseId)) Left(NotFoundException(elemNotFoundErrMsg))
    Right(catalogRepository.findCatalogByStudentAndCourse(studentId, courseId))
  }

  /**
   * Update the graade that a student has received for a specific course.
   * @param student the student who received the grade
   * @param course the course for which the grade has been obtained
   * @param grade the grade as a number
   * @return the grade as a class.
   */
  def updateGrade(student: Student, course: Course, grade: Double): Catalog = {
    val catalog = new Catalog(student, course)
    catalog.setGrade(grade)
    catalogRepository.save(catalog)
  }

}
