package com.catalog.app.services

import com.catalog.app.config.NotFoundException
import org.springframework.data.jpa.repository.JpaRepository

import java.lang
import scala.jdk.OptionConverters.RichOptional

abstract class BasicService[T](repository: JpaRepository[T, Integer],
                               elem: String) {

  val elemNotFoundErrMsg: String = "This " + elem + " does not exist inside the database."

  /**
   * Query all the elements of type T inside the db.
   * @return all the elements of type T inside the db.
   */
  def getAll: lang.Iterable[T] = {
    repository.findAll()
  }

  /**
   * Query one element from the db.
   * @param id the id of the element to look for.
   * @return the information about this element.
   */
  def getOne(id: Int): Either[Exception, T]= {
    repository.findById(id).toScala match {
      case None => Left(NotFoundException(elemNotFoundErrMsg))
      case Some(user) => Right(user)
    }
  }

}
