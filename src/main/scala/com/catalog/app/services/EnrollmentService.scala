package com.catalog.app.services

import com.catalog.app.config.{AlreadyExistsException, BadPropertyException, NotFoundException, UnauthorizedException}
import com.catalog.app.entities.{FacultyMember, User, UserRole}
import com.catalog.app.repositories.UserRepository
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Service

import scala.jdk.OptionConverters.RichOptional

class EnrollmentService[T <: FacultyMember](repository: JpaRepository[T, Integer],
                                            courseService: CourseService,
                                            userRepository: UserRepository,
                                            elem: String)
  extends BasicService[T](repository, elem) {

  val alreadyEnrolled: String = "This " + elem + " is already enrolled in this course."


  def createAccount(newElement: T, userRole: UserRole): Either[Exception, T] = {
    if(userRole == UserRole.ADMIN) return Left(UnauthorizedException("Cannot create other admins!"))
    if(newElement.getUser == null) return Left(BadPropertyException("Missing user details for the new account."))
    val valid = validateAccount(newElement.getUser)
    valid match {
      case Some(exception: Exception) => Left(exception)
      case None =>
        newElement.getUser.setEnabled(true)
        newElement.getUser.setUserRole(userRole)
        Right(repository.save(newElement))
    }
  }

  def validateAccount(newAccount: User): Option[Exception] = {
    if (newAccount.getUsername == null || newAccount.getUsername.isBlank) return Some(BadPropertyException("The username of the new account cannot be blank."))
    if (userRepository.existsUserByUsername(newAccount.getUsername)) return Some(AlreadyExistsException("The username is already in use by another user. Please choose another one."))
    if (newAccount.getPassword == null || newAccount.getPassword.isBlank) return Some(BadPropertyException("The password of the new account cannot be blank."))
    None
  }

  def enrollInCourse(memberId: Int, courseId: Int): Either[Exception, T]= {
    repository.findById(memberId).toScala match {
      case None => Left(NotFoundException(elemNotFoundErrMsg))
      case Some(member) => courseService.getOne(courseId) match {
        case Left(e) =>Left(e)
        case Right(course) =>
          if (member.alreadyEnrolled(courseId)) return Left(BadPropertyException(alreadyEnrolled))
          member.enroll(course)
          Right(repository.save(member))
      }
    }
  }

  def unenrollFromCourse(memberId: Int, courseId: Int): Either[Exception, T] = {
    repository.findById(memberId).toScala match {
      case None => Left(NotFoundException(elemNotFoundErrMsg))
      case Some(memeber) => courseService.getOne(courseId) match {
        case Left(e) => Left(e)
        case Right(course) =>
          if (!memeber.alreadyEnrolled(courseId)) return Left(BadPropertyException(alreadyEnrolled))
          memeber.unenroll(course)
          Right(repository.save(memeber))
      }
    }  }

}
