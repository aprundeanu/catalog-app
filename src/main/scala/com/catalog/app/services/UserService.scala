package com.catalog.app.services

import com.catalog.app.config.{NotFoundException, UnauthorizedException}
import com.catalog.app.entities.{User, UserRole}
import com.catalog.app.repositories.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import java.lang
import java.util.Optional
import scala.jdk.OptionConverters.RichOptional

@Service
class UserService(@Autowired private val userRepository: UserRepository) extends BasicService[User](userRepository, elem = "user"){

  /**
   * Change some account information for an user.
   * @param id the id of the user.
   * @param user new information to replace the old one (username / password etc.)
   * @return the updated user.
   */
  def editUser(id: Integer, user: User): Either[Exception, User] = {
    userRepository.findById(id).toScala match {
      case None => Left(NotFoundException(elemNotFoundErrMsg))
      case Some(oldUser) =>
        if(!user.getUsername.isBlank) oldUser.setUsername(user.getUsername)
        if(!user.getPassword.isBlank) oldUser.setPassword(user.getPassword)
        Right(userRepository.save(oldUser))
    }
  }

  /**
   * Change the active status of an user account.
   * @param id the id of the user.
   * @param active flag to indicate if the account needs to be (de)activated
   * @return the updated user account.
   */
  def changeActiveStatus(id: Integer, active: Boolean): Either[Exception, User] = {
    userRepository.findById(id).toScala match {
      case None => Left(NotFoundException(elemNotFoundErrMsg))
      case Some(user) =>
        user.setEnabled(active)
        Right(userRepository.save(user))
    }
  }

  /**
   * Remove an account from the database.
   * @param id the id of the user to have the account deleted.
   * @return status message.
   */
  def deleteAccount(id: Integer): Either[Exception, String] = {
    userRepository.findById(id).toScala match {
      case None => Left(NotFoundException(elemNotFoundErrMsg))
      case Some(user) =>
        if(user.getUserRole.name.equals(UserRole.ADMIN.name())) return Left(UnauthorizedException("Cannot delete account of another admin."))
        userRepository.deleteById(id)
        Right("Deleted")
    }
  }

}
