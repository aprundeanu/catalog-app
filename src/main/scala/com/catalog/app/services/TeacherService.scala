package com.catalog.app.services

import com.catalog.app.entities.{Teacher, User}
import com.catalog.app.repositories.{TeacherRepository, UserRepository}
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import scala.jdk.OptionConverters.RichOptional

@Service
class TeacherService(@Autowired private val teacherRepository: TeacherRepository,
                     @Autowired private val courseService: CourseService,
                     @Autowired private val userRepository: UserRepository)
  extends EnrollmentService[Teacher](teacherRepository, courseService, userRepository, elem = "teacher"){

}
