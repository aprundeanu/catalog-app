package com.catalog.app

object Assignment {

  def reverse(input: List[String]): List[String] = input match {
    case head :: Nil => List(processStringReversal(head))
    case head :: tail =>reverse(tail) ++ List(processStringReversal(head))
  }

  def processStringReversal(s: String): String = {
    reverseString(s.toList).mkString
  }

  def reverseString(word: List[Char]): List[Char] = word match {
    case head :: Nil => List(head)
    case head :: tail => reverseString(tail) ++ List(head)
  }

  def counting(input: List[List[String]]): List[(String, Int)] = {
    val list = input.flatten
    list.groupBy(elem => elem).map(x => (x._1, x._2.length)).toList.sortBy(t => - t._2)
  }

  def main(args: Array[String]): Unit = {
    var inp: List[String] = List("come", "leave")
    println(reverse(inp))
//    println(processStringReversal("come"))
    var input2: List[List[String]] = List(
      List("Navid", "Mehdi", "Alex", "Arniel", "Lotte"),
      List("Ben" , "David", "Arniel"),
      List("David", "Mehdi", "Arniel", "Sara"))
    println(counting(input2))
  }

}
