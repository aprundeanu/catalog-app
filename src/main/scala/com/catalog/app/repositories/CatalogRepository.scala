package com.catalog.app.repositories

import com.catalog.app.entities.{Catalog, CatalogKey}
import org.springframework.data.jpa.repository.{JpaRepository, Query}
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

import java.lang.Iterable
import java.util

@Repository
trait CatalogRepository extends JpaRepository[Catalog, CatalogKey]{

  @Query("SELECT c FROM Catalog c WHERE c.catalogKey.courseId = :course_id")
  def findCatalogsByCourseId(@Param("course_id")courseId: Integer): util.List[Catalog]

  @Query("SELECT c FROM Catalog c WHERE c.catalogKey.studentId = :student_id")
  def findCatalogsByStudentId(@Param("student_id")studentId: Integer): util.List[Catalog]

  @Query("select c from Catalog c where c.catalogKey.studentId = :student_id and c.catalogKey.courseId = :course_id")
  def findCatalogByStudentAndCourse(@Param("student_id") studentId: Integer,
                                    @Param("course_id") courseId: Integer): Catalog

}
