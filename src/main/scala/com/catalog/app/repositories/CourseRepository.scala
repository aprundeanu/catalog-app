package com.catalog.app.repositories

import com.catalog.app.entities.Course
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
trait CourseRepository extends JpaRepository[Course, Integer]{

}
