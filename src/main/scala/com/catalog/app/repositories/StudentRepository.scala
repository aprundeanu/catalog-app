package com.catalog.app.repositories

import com.catalog.app.entities.Student
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

import java.util.Optional

@Repository
trait StudentRepository extends JpaRepository[Student, Integer]{
}
