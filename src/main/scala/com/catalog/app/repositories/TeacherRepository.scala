package com.catalog.app.repositories

import com.catalog.app.entities.Teacher
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
trait TeacherRepository extends JpaRepository[Teacher, Integer]{
}
