package com.catalog.app.repositories

import com.catalog.app.entities.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

import java.util.Optional

@Repository
trait UserRepository extends JpaRepository[User, Integer] {

  def existsUserByUsername(username: String): Boolean

  def findByUsername(username: String): Optional[User]
}