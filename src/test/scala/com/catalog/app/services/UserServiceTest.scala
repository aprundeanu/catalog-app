package com.catalog.app.services;

import com.catalog.app.config.UnauthorizedException
import com.catalog.app.entities.{User, UserRole}
import com.catalog.app.repositories.UserRepository
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.{BeforeAll, BeforeEach, Test, TestInstance}
import org.mockito.ArgumentMatchers.{any, anyInt}
import org.mockito.Mockito.{doNothing, mock, when}
import org.mockito.MockitoAnnotations

import java.util
import java.util.Optional;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserServiceTest {

  var userRepository: UserRepository = _
  var userService: UserService = _
  var user: User = new User

  @BeforeAll
  def setup(): Unit = {
    var users: java.util.List[User] = new util.ArrayList[User]();
    users.add(user);
    userRepository = mock(classOf[UserRepository])
    when(userRepository.findById(anyInt())).thenReturn(Optional.of(user))
    when(userRepository.findAll()).thenReturn(users)
    when(userRepository.save(any())).thenReturn(user)
    doNothing().when(userRepository).deleteById(anyInt())
    userService = new UserService(userRepository)
  }

  @BeforeEach
  def setupUser(): Unit = {
    user.setId(1)
    user.setUsername("Bob")
    user.setPassword("BOB")
    user.setUserRole(UserRole.STUDENT)
    user.setEnabled(true)
  }

    @Test
    def deleteAccountTest(): Unit = {
      assert(userService.deleteAccount(1) == Right("Deleted"))
    }

    @Test
    def deleteAccountUnauthorizedTest(): Unit = {
      user.setUserRole(UserRole.ADMIN)
      assertEquals(userService.deleteAccount(1), Left(UnauthorizedException("Cannot delete account of another admin.")))
    }

    @Test
    def changeActiveStatusTest(): Unit = {
      var u = new User
      u.setId(1)
      u.setUsername(user.getUsername)
      u.setPassword(user.getPassword)
      u.setEnabled(true)
      u.setUserRole(UserRole.STUDENT)
      user.setEnabled(false)
      assert(userService.changeActiveStatus(1, true).isRight)
      assert(userService.changeActiveStatus(1, true) == Right(u))
    }

    @Test
    def editUserTest(): Unit = {
      assertEquals(userService.editUser(1, user), Right(user))
    }
}