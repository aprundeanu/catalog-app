package com.catalog.app.services

import com.catalog.app.config.{AlreadyExistsException, BadPropertyException, UnauthorizedException}
import com.catalog.app.entities.{Course, Teacher, User, UserRole}
import com.catalog.app.repositories.{CatalogRepository, CourseRepository, TeacherRepository, UserRepository}
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.{BeforeAll, Test, TestInstance}
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.{any, anyInt, anyString}
import org.mockito.{Mock, MockitoAnnotations}
import org.mockito.Mockito.{doReturn, mock}
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.boot.test.mock.mockito.MockBean

import java.util.Optional

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class EnrollmentServiceTest {

  var teacherRepository: TeacherRepository = _
  var courseRepository: CourseRepository = _
  var catalogRepository: CatalogRepository = _
  var userRepository: UserRepository = _
  var teacherService: TeacherService = _
  var teacher: Teacher = new Teacher

  @BeforeAll
  def setup(): Unit = {
    userRepository = mock(classOf[UserRepository])
    teacherRepository = mock(classOf[TeacherRepository])

    courseRepository = mock(classOf[CourseRepository])
    catalogRepository = mock(classOf[CatalogRepository])

    val courseService = new CourseService(courseRepository, catalogRepository)
    teacherService = new TeacherService(teacherRepository, courseService, userRepository)
    teacher.setId(1)
    val u = new User
    u.setUsername("Mr. Smith")
    u.setPassword("password")
    u.setEnabled(true)
    teacher.setUser(u)
  }

  @Test
  def createAccountTest(): Unit = {
    val teacher2 = new Teacher
    val u2 = new User
    u2.setUsername("Mr. Smith")
    u2.setPassword("password")
    u2.setEnabled(false)
    teacher2.setUser(u2)
    doReturn(teacher2).when(teacherRepository).save(any())
    assertEquals(teacherService.createAccount(teacher2, userRole = UserRole.TEACHER), Right(teacher))
  }

  @Test
  def createAccountUnauthorizedTest(): Unit = {
    assertEquals(teacherService.createAccount(teacher, userRole = UserRole.ADMIN),
      Left(UnauthorizedException("Cannot create other admins!")))
  }

  @Test
  def createAccountUsernameFailTest(): Unit = {
    assertEquals(teacherService.createAccount(teacher, userRole = UserRole.STUDENT),
      Left(BadPropertyException("Missing user details for the new account.")))
  }

  @Test
  def createAccountUsernameExistsFailTest(): Unit = {
    doReturn(true).when(userRepository).existsUserByUsername(anyString())
    val u = new User
    u.setUsername("Mr. Smith")
    teacher.setUser(u)
    assertEquals(teacherService.createAccount(teacher, userRole = UserRole.STUDENT),
      Left(AlreadyExistsException("The username is already in use by another user. Please choose another one.")))
  }

  @Test
  def createAccountEmptyPasswordFailTest(): Unit = {
    doReturn(false).when(userRepository).existsUserByUsername(anyString())
    val u = new User
    u.setUsername("Mr. Smith")
    u.setPassword("")
    teacher.setUser(u)
    assertEquals(teacherService.createAccount(teacher, userRole = UserRole.STUDENT),
      Left(BadPropertyException("The password of the new account cannot be blank.")))
  }

  @Test
  def enrollInCourseTest(): Unit = {
    val teacher2 = new Teacher
    val u2 = new User
    u2.setUsername("Mr. Smith")
    u2.setPassword("password")
    u2.setEnabled(true)
    teacher2.setUser(u2)

    val c = new Course
    c.setId(1)
    c.setName("History")
    c.setEdition(1)
    teacher2.getCourses.add(c)

    doReturn(Optional.of(teacher)).when(teacherRepository).findById(anyInt())
    doReturn(teacher2).when(teacherRepository).save(any())
    doReturn(Optional.of(c)).when(courseRepository).findById(anyInt())
    teacherService.enrollInCourse(1, 1) match {
      case Right(t) => assertEquals(t, teacher2)
    }
  }

  @Test
  def enrollInCourseAlreadyEnrolledFailTest(): Unit = {
    val teacher2 = new Teacher
    val u2 = new User
    u2.setUsername("Mr. Smith")
    u2.setPassword("password")
    u2.setEnabled(true)
    teacher2.setUser(u2)

    val c = new Course
    c.setId(1)
    c.setName("History")
    c.setEdition(1)
    teacher2.getCourses.add(c)

    doReturn(Optional.of(teacher2)).when(teacherRepository).findById(anyInt())
    doReturn(teacher2).when(teacherRepository).save(any())
    doReturn(Optional.of(c)).when(courseRepository).findById(anyInt())
    assertEquals(teacherService.enrollInCourse(1, 1), Left(BadPropertyException("This teacher is already enrolled in this course.")))
  }

}