package com.catalog.app.controllers

import com.catalog.app.config.NotFoundException
import com.catalog.app.entities.User
import com.catalog.app.services.UserService
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.{BeforeAll, Test, TestInstance}
import org.mockito.ArgumentMatchers.{any, anyBoolean, anyInt}
import org.mockito.Mockito.doReturn
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.{MockMvc, ResultActions}
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.{content, status}
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext

@WebMvcTest(controllers = Array(classOf[UserController]))
@WebAppConfiguration
@ContextConfiguration
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserControllerTest {

  @Autowired var context: WebApplicationContext = _
  @Autowired var mapper: ObjectMapper = _

  @MockBean var userService: UserService = _
  var mockMvc: MockMvc = _
  val user: User = new User()

  @BeforeAll
  def setUp(): Unit = {
    mockMvc = MockMvcBuilders.webAppContextSetup(context).build()
    user.setId(1)
    user.setUsername("Bob")
    user.setPassword("Password")
    user.setEnabled(true)
  }

  @Test
  def editUserTest(): Unit = {
    doReturn(Right(user)).when(userService).editUser(anyInt(), any[User]())
    mockMvc.perform(put("/users/1")
      .contentType(MediaType.APPLICATION_JSON)
      .content(mapper.writeValueAsString(user))
      .accept(MediaType.APPLICATION_JSON))
      .andExpect(status.isOk)
      .andExpect(content.contentType(MediaType.APPLICATION_JSON))
      .andExpect(content().json(mapper.writeValueAsString(user)))
  }

  @Test
  def editUserFailTest(): Unit = {
    doReturn(Left(NotFoundException("This user does not exist inside the database.")))
      .when(userService).editUser(anyInt(), any[User]())
    mockMvc.perform(put("/users/1")
      .contentType(MediaType.APPLICATION_JSON)
      .content(mapper.writeValueAsString(user))
      .accept(MediaType.APPLICATION_JSON))
      .andExpect(status.isNotFound)
  }

  @Test
  def activateAccountTest(): Unit = {
    doReturn(Right(user)).when(userService).changeActiveStatus(anyInt(), anyBoolean())
    mockMvc.perform(put("/users/1/activate")
      .contentType(MediaType.APPLICATION_JSON)
      .content(mapper.writeValueAsString(user))
      .accept(MediaType.APPLICATION_JSON))
      .andExpect(status.isOk)
      .andExpect(content.contentType(MediaType.APPLICATION_JSON))
      .andExpect(content().json(mapper.writeValueAsString(user)))
  }

}
