package com.catalog.app.controllers

import com.catalog.app.entities.{Teacher, User, UserRole}
import com.catalog.app.services.TeacherService
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.{BeforeAll, Test, TestInstance}
import org.mockito.ArgumentMatchers
import org.mockito.ArgumentMatchers.{any, anyInt}
import org.mockito.Mockito.doReturn
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.{post, put}
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.{content, status}
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext

@WebMvcTest(controllers = Array(classOf[TeacherController]))
@WebAppConfiguration
@ContextConfiguration
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class EnrollmentControllerTest {

  @Autowired var context: WebApplicationContext = _
  @Autowired var mapper: ObjectMapper = _

  @MockBean var teacherService: TeacherService = _
  var mockMvc: MockMvc = _
  val user: Teacher = new Teacher()

  @BeforeAll
  def setUp(): Unit = {
    mockMvc = MockMvcBuilders.webAppContextSetup(context).build()
    user.setId(1)
    user.setUsername("Bob")
  }

  @Test
  def createAccountTest(): Unit = {
    doReturn(Right(user)).when(teacherService).createAccount(any(), ArgumentMatchers.eq(UserRole.TEACHER))
    mockMvc.perform(post("/teachers")
      .contentType(MediaType.APPLICATION_JSON)
      .content(mapper.writeValueAsString(user))
      .accept(MediaType.APPLICATION_JSON))
      .andExpect(status.isCreated)
      .andExpect(content.contentType(MediaType.APPLICATION_JSON))
      .andExpect(content().json(mapper.writeValueAsString(user)))
  }

  @Test
  def enrollInCourseTest(): Unit = {
    doReturn(Right(user)).when(teacherService).enrollInCourse(anyInt(), anyInt())
    mockMvc.perform(put("/teachers/1/enroll/1"))
      .andExpect(status.isOk)
      .andExpect(content.contentType(MediaType.APPLICATION_JSON))
      .andExpect(content().json(mapper.writeValueAsString(user)))
  }

  @Test
  def unenrollInCourseTest(): Unit = {
    doReturn(Right(user)).when(teacherService).unenrollFromCourse(anyInt(), anyInt())
    mockMvc.perform(put("/teachers/1/unenroll/1"))
      .andExpect(status.isOk)
      .andExpect(content.contentType(MediaType.APPLICATION_JSON))
      .andExpect(content().json(mapper.writeValueAsString(user)))
  }

}
